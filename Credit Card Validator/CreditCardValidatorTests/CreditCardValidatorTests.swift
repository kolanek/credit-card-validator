//
//  CreditCardValidatorTests.swift
//  CreditCardValidatorTests
//
//  Created by Bartosz Kolanek on 26.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import XCTest
@testable import Credit_Card_Validator

class CreditCardValidatorTests: XCTestCase {
    
    func testCheckSum(){
        var number = "5169310000871482"
        let newCreditCard = CreditCard()
        var checkSumBool = newCreditCard.checkSum(number: number)
        
        XCTAssertTrue(checkSumBool)
        
        number = "5169310000871483"
        checkSumBool = newCreditCard.checkSum(number: number)
        
        XCTAssertFalse(checkSumBool)
    }
    
    func testGenerateNumber(){
        let newCreditCard = CreditCard()
        let newNumber = newCreditCard.generateNumber(length: 2, range: 10)
        
        XCTAssertLessThan(Int(newNumber)!, 99)
        
    }
    
    func testGenerateCreditCardDate(){
        let newCreditCard = CreditCard()
        let date = newCreditCard.date
        let index = date.index(date.startIndex, offsetBy: 2)
        let month = date[..<index]
        XCTAssertLessThan(Int(date)!, 1299)
        XCTAssertGreaterThan(Int(month)!, 0)
    }
    
    func testGenerateCreditCardNumber(){
        let newCreditCard = CreditCard()
        let number = newCreditCard.number
        
        XCTAssertLessThanOrEqual(Int(number)!, 9999999999999999)
        XCTAssertEqual(number.count, 16)
    }
    
    func testGenerateCreditCardCVC(){
        let newCreditCard = CreditCard()
        let cvc = newCreditCard.cvc
        
        XCTAssertLessThanOrEqual(Int(cvc)!, 999)
        XCTAssertGreaterThanOrEqual(Int(cvc)!, 0)
    }
    
}
