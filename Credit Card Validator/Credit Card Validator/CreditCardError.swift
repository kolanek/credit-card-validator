//
//  CreditCardError.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 26.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation

struct CreditCardError: Decodable {
    let error: String
    let message: String
    
    init(error: String, message: String){
        self.error = error
        self.message = message
    }
}
