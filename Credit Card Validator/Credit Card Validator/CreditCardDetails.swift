//
//  CreditCardDetails.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 26.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation

struct CreditCardDetails: Decodable {
    let bank: String
    let bin: String
    let card: String
    let country: String
    let countrycode: String
    let level: String
    let phone: String
    let type: String
    let valid: String
    let website: String
    
    init(){
        bank = ""
        bin = ""
        card = ""
        country = ""
        countrycode = ""
        level = ""
        phone = ""
        type = ""
        valid = ""
        website = ""
    }
}
