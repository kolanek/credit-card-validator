//
//  Colors.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 26.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit

enum Colors {
    case green
    case red
    
    var tint: UIColor {
        switch self {
        case .green:
            return UIColor(red: 0/255, green: 195/255, blue: 79/255, alpha: 1)
        case .red:
            return UIColor(red: 244/255, green: 0, blue: 50/255, alpha: 1)
        }
    }
}
