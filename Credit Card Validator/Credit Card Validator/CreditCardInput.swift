//
//  CreditCardInput.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 28.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit

protocol CreditCardInputDelegate {
    func creditCardInputIsEditing(_ editing: Bool)
}

class CreditCardInput: UIView, UITextFieldDelegate {
    
    lazy var creditCardNumberTextField: UITextField! = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.borderStyle = .roundedRect
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var creditCardDateTextField: UITextField! = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.borderStyle = .roundedRect
        view.textAlignment = .center
        
        return view
    }()
    
    lazy var creditCardCVCTextField: UITextField! = {
        let view = UITextField()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.borderStyle = .roundedRect
        view.textAlignment = .center
        
        return view
    }()
    
    var cciDelegate: CreditCardInputDelegate!
    
    var textFieldStackView = UIStackView()
    var buttonStackView = UIStackView()
    var formStackView = UIStackView()
    let generateButton = UIButton()
    let validateButton = UIButton()
    let actIndContainer = UIView()
    let loadingView = UIView()
    let actInd = UIActivityIndicatorView()
    var alertService = AlertService()
    
    var viewController: UIViewController!
    
    var isOnOriginPosition = true
    var creditCard = CreditCard(number: "", date: "", cvc: "")
    var stackViewIsOnInitialPosition = true
    
    init(){
        super.init(frame: UIScreen.main.bounds);
    }
    
    init(vc: UIViewController, width: Int, height: Int) {
        super.init(frame: CGRect(x: 0, y: 0, width: width, height: height));
        
        viewController = vc        
        
        creditCardNumberTextField.placeholder = "0000 0000 0000 0000"
        
        creditCardDateTextField.placeholder = "MM/YY"
        
        creditCardCVCTextField.placeholder = "CVC"
        
        textFieldStackView = UIStackView(arrangedSubviews: [creditCardDateTextField, creditCardCVCTextField])
        textFieldStackView.axis = .horizontal
        textFieldStackView.spacing = 5
        textFieldStackView.distribution = .fillEqually
        
        validateButton.setTitle("Validate", for: .normal)
        validateButton.backgroundColor = Colors.green.tint
        validateButton.setTitleColor(.white, for: .normal)
        validateButton.layer.cornerRadius = 5
        validateButton.addTarget(self, action: #selector(validateButtonClicked), for: .touchUpInside)
        
        generateButton.setTitle("Generate", for: .normal)
        generateButton.backgroundColor = Colors.green.tint
        generateButton.setTitleColor(.white, for: .normal)
        generateButton.layer.cornerRadius = 5
        generateButton.addTarget(self, action: #selector(generateButtonClicked), for: .touchUpInside)
        
        buttonStackView = UIStackView(arrangedSubviews: [generateButton, validateButton])
        buttonStackView.translatesAutoresizingMaskIntoConstraints = false
        buttonStackView.axis = .horizontal
        buttonStackView.spacing = 5
        buttonStackView.distribution = .fillEqually
        
        
        formStackView = UIStackView(arrangedSubviews: [creditCardNumberTextField, textFieldStackView, buttonStackView])
        formStackView.translatesAutoresizingMaskIntoConstraints = false
        formStackView.axis = .vertical
        formStackView.spacing = 5
        formStackView.distribution = .fillEqually
        
        addSubview(formStackView)
        formStackView.widthAnchor.constraint(equalTo: widthAnchor, multiplier: 1).isActive = true
        formStackView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 1).isActive = true
        formStackView.topAnchor.constraint(equalTo: topAnchor).isActive = true
        formStackView.bottomAnchor.constraint(equalTo: bottomAnchor).isActive = true
        formStackView.leftAnchor.constraint(equalTo: leftAnchor).isActive = true
        formStackView.rightAnchor.constraint(equalTo: rightAnchor).isActive = true

        creditCardNumberTextField.delegate = self
        creditCardNumberTextField.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        creditCardNumberTextField.keyboardType = UIKeyboardType.numberPad
        
        creditCardDateTextField.delegate = self
        creditCardDateTextField.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        creditCardDateTextField.keyboardType = UIKeyboardType.numberPad
        
        creditCardCVCTextField.delegate = self
        creditCardCVCTextField.addTarget(self, action: #selector(didChangeText(textField:)), for: .editingChanged)
        creditCardCVCTextField.keyboardType = UIKeyboardType.numberPad
        
        actIndContainer.frame = self.frame
        actIndContainer.center = self.center
        actIndContainer.backgroundColor = UIColor.white.withAlphaComponent(0.3)
        actIndContainer.isHidden = true
        
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = self.center
        loadingView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        actInd.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height:40.0);
        actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        actInd.center = CGPoint(x: loadingView.frame.size.width / 2, y: loadingView.frame.size.height / 2);
        
        loadingView.addSubview(actInd)
        actIndContainer.addSubview(loadingView)
        actIndContainer.isUserInteractionEnabled = false
        
        self.addSubview(actIndContainer)
        
        self.addDoneButtonOnKeyboard()
    }
    
    
    @objc func generateButtonClicked(sender: UIButton!){
        creditCard = CreditCard()
        
        creditCardNumberTextField.text = creditCard.number.formatTo(format: .creditCard)
        creditCardDateTextField.text = creditCard.date.formatTo(format: .creditCardDate)
        creditCardCVCTextField.text = creditCard.cvc
        
        validateTextFields()
    }
    
    @objc func validateButtonClicked(sender: UIButton!){
        self.endEditing(true)
        validateTextFields()
        
        
        if creditCard.number != "" && creditCard.date != "" && creditCard.cvc != "" {
            self.activityIndicator(show: true)
            creditCard.validate() { [unowned self]  result in
                
                self.activityIndicator(show: false)
                let theStruct = TheStruct()
                
                if theStruct.checkType(result, isType: CreditCardDetails.self) {
                    let details = result as! CreditCardDetails
                    let message = "It's a \(details.type) card that belongs to \(details.bank) \n located in \(details.country)"
                    self.alertService.simpleAlert(name: "Card number correct", message: message, vc: self.viewController)
                    
                }
                if theStruct.checkType(result, isType: CreditCardError.self) {
                    let error = result as! CreditCardError
                    var errorName = error.error
                    errorName = errorName == "No internet connection" ? errorName : "Card number incorrect"
                    self.alertService.simpleAlert(name: errorName, message: error.message, vc: self.viewController)
                }
            }
        }
    }
    
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: 320, height: 50))
        doneToolbar.barStyle = UIBarStyle.default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.done, target: self, action: #selector(CreditCardInput.doneButtonAction))
        
        var items = [UIBarButtonItem]()
        items.append(flexSpace)
        items.append(done)
        
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.creditCardNumberTextField.inputAccessoryView = doneToolbar
        self.creditCardDateTextField.inputAccessoryView = doneToolbar
        self.creditCardCVCTextField.inputAccessoryView = doneToolbar
        
    }
    
    @objc func doneButtonAction(textField:UITextField) {
        self.endEditing(true)
    }
    
    func validateTextFields(){
        creditCardCVCTextField.backgroundColor = creditCard.cvc == "" ? Colors.red.tint : UIColor.white
        creditCardDateTextField.backgroundColor = creditCard.date == "" ? Colors.red.tint : UIColor.white
        creditCardNumberTextField.backgroundColor = creditCard.number == "" ? Colors.red.tint : UIColor.white
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        textField.backgroundColor = UIColor.white
        cciDelegate.creditCardInputIsEditing(true)
        //        if stackViewIsOnInitialPosition {
        //            stackViewIsOnInitialPosition = moveView(formStackView, moveDistance: -150, up: true)
        //        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        cciDelegate.creditCardInputIsEditing(false)
        //        if !stackViewIsOnInitialPosition {
        //            stackViewIsOnInitialPosition = moveView(formStackView, moveDistance: -150, up: false)
        //        }
        
        if textField == creditCardNumberTextField {
            if textField.text?.count == 19{
                creditCard.number = creditCardNumberTextField.text!.replacingOccurrences(of: " ", with: "")
                creditCardDateTextField.becomeFirstResponder()
            }else{
                creditCard.number = ""
                textField.backgroundColor = Colors.red.tint
            }
        }
        if textField == creditCardDateTextField {
            if textField.text?.count == 5 {
                creditCard.date = creditCardDateTextField.text!
                creditCardCVCTextField.becomeFirstResponder()
            }else{
                creditCard.date = ""
                textField.backgroundColor = Colors.red.tint
            }
        }
        if textField == creditCardCVCTextField {
            if textField.text?.count == 3 {
                creditCard.cvc = creditCardCVCTextField.text!
            }else{
                creditCard.cvc = ""
                textField.backgroundColor = Colors.red.tint
            }
        }
    }
    
    @objc func didChangeText(textField:UITextField) {
        if textField == creditCardNumberTextField {
            textField.text = textField.text?.formatTo(format: .creditCard)
            if textField.text?.count == 19 {
                self.endEditing(true)
            }
        }
        if textField == creditCardDateTextField {
            let input = textField.text!
            if input.count==1{
                let index = input.index(input.startIndex, offsetBy: 1)
                let month = input[..<index]
                if Int(month)! > 1 {
                    textField.text = "0\(month)"
                }
            }
            
            textField.text = textField.text?.formatTo(format: .creditCardDate)
            if textField.text?.count == 5 {
                self.endEditing(true)
            }
            if textField.text?.count == 0 {
                creditCardNumberTextField.becomeFirstResponder()
            }
        }
        if textField == creditCardCVCTextField {
            if textField.text?.count == 3 {
                self.endEditing(true)
            }
            if textField.text?.count == 0 {
                creditCardDateTextField.becomeFirstResponder()
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let newLength = textField.text!.count + string.count - range.length
        
        if textField == creditCardNumberTextField {
            return newLength <= 19
        }
        if textField == creditCardDateTextField {
            return newLength <= 5
        }
        if textField == creditCardCVCTextField {
            return newLength <= 3
        }
        
        return true
    }
    
    // MARK: - Indicator
    
    func activityIndicator(show: Bool) {
        if show {
            actIndContainer.isHidden = false
            actInd.startAnimating()
        }else {
            actIndContainer.isHidden = true
            actInd.stopAnimating()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
