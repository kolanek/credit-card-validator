//
//  TheStruct.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 28.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation

class TheStruct {
    
    func checkType<T: Any>(_ item: Any, isType: T.Type) -> Bool {
        return (item as? T) != nil
    }
}
