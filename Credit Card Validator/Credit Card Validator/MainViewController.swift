//
//  MainViewController.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 24.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UITextFieldDelegate, CreditCardInputDelegate {
    
    var ccInput = CreditCardInput()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ccInput = CreditCardInput(vc: self, width: 300, height: 150)
        ccInput.center = view.center
        ccInput.cciDelegate = self
        
        view.addSubview(ccInput)
    }
    
    // MARK: - StackView movement animation
    
    func creditCardInputIsEditing(_ editing: Bool) {
        if editing && ccInput.isOnOriginPosition{
            ccInput.isOnOriginPosition = moveView(ccInput, moveDistance: -150, up: true)
        }else {
            ccInput.isOnOriginPosition = moveView(ccInput, moveDistance: -150, up: false)
        }
    }
    
    func moveView(_ view: UIView, moveDistance: Int, up: Bool) -> Bool {
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance : -moveDistance)
        
        UIView.beginAnimations("animateStackView", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
        return !up
    }
}
