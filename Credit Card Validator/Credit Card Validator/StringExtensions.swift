//
//  StringExtensions.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 24.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation

enum Format {
    case creditCard
    case creditCardDate
}

extension String {
    
    func formatTo(format: Format) -> String {
        let modulo = format == .creditCard ? 4 : 2
        let appendChar = format == .creditCard ? " " : "/"
        let cardNumber = self
        let trimmedString = cardNumber.components(separatedBy: appendChar).joined()
        let arrOfCharacters = Array(trimmedString)
        var modifiedCreditCardString = ""
    
        if(arrOfCharacters.count > 0) {
            for i in 0...arrOfCharacters.count-1 {
                modifiedCreditCardString.append(arrOfCharacters[i])
                if((i+1) % modulo == 0 && i+1 != arrOfCharacters.count) {
                    modifiedCreditCardString.append(appendChar)
                }
            }
        }
        return modifiedCreditCardString
    }
}
