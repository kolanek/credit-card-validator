//
//  AlertService.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 28.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation
import UIKit
struct AlertService {
    
    func simpleAlert(name: String, message: String, vc: UIViewController){
        let alert = UIAlertController(title: name, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        vc.present(alert, animated: true)
    }
    
}
