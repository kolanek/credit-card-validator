//
//  CreditCard.swift
//  Credit Card Validator
//
//  Created by Bartosz Kolanek on 26.05.2018.
//  Copyright © 2018 Bartosz Kolanek. All rights reserved.
//

import Foundation

class CreditCard {
    internal var number: String = ""
    internal var date: String = ""
    internal var cvc: String = ""
    var bincodeAPIKey = "5232a9bca11e25c0f8eb4313ff2644be"
    var reachability = Reachability.init()
    
    init(number: String, date: String, cvc: String){
        self.number = number
        self.date = date
        self.cvc = cvc
    }
    
    init(){
        self.cvc = generateCreditCardCVC()
        self.date = generateCreditCardDate()
        self.number = generateCreditCardNumber()
    }
    
    func generateCreditCardNumber() -> String{
        var number = ""
        repeat {
            number = generateNumber(length: 16, range: 10)
        } while (!checkSum(number: number))
        return number
    }
    
    func generateCreditCardCVC() -> String {
        return generateNumber(length: 3, range: 10)
    }
    
    func generateCreditCardDate() -> String {
        let firstDigit = generateNumber(length: 1, range: 2)
        var secondDigit = ""
        repeat {
            secondDigit = firstDigit == "0" ? generateNumber(length: 1, range: 10) : generateNumber(length: 1, range: 3)
        } while (secondDigit == "0")
        
        let month = "\(firstDigit)\(secondDigit)"
        let year = generateNumber(length: 2, range: 10)
        
        return "\(month)\(year)"
    }
    
    func generateNumber(length: Int, range: UInt32) -> String{
        var number = ""
        for _ in 1...length{
            let randomNumber = arc4random_uniform(range);
            number += "\(randomNumber)"
        }
        return number
    }
    
    func checkSum(number: String) -> Bool{
        
        var digitPosition = 1
        var checkSumArray: [Int] = []
        var sum = 0
        
        for i in number {
            if digitPosition%2 == 0{
                checkSumArray.append(Int(String(i))!)
            }else{
                let scalar = Int(String(i))!*2
                if scalar < 10 {
                    checkSumArray.append(scalar)
                }else{
                    let stringScalar = "\(scalar)"
                    var newNumber = 0
                    for j in stringScalar {
                        newNumber += Int(String(j))!
                    }
                    checkSumArray.append(newNumber)
                }
            }
            digitPosition += 1
        }
        
        for i in checkSumArray{
            sum += i
        }
        
        return sum%10==0 ? true : false
    }
    
    func validate(resultBlock: @escaping (Any)->Void){
        
        if self.reachability!.connection == .cellular || self.reachability!.connection == .wifi {
            let jsonUrlString = "https://api.bincodes.com/cc/json/\(bincodeAPIKey)/\(self.number)/"
            guard let url = URL(string: jsonUrlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                let httpResponse = response as? HTTPURLResponse
                let status = httpResponse?.statusCode ?? 0
                
                if status == 200 {
                    DispatchQueue.main.async {
                        do {
                            guard let result = try? JSONDecoder().decode(CreditCardDetails.self, from: data) else{
                                let result = try JSONDecoder().decode(CreditCardError.self, from: data)
                                resultBlock(result)
                                return
                            }
                            resultBlock(result)
                        }catch let jsonErr{
                            print("Error in serializing json: \(jsonErr)")
                        }
                    }
                }else {
                    resultBlock(CreditCardError(error: "Connection ERROR", message: "Somethin went wrong"))
                    return
                }
                }.resume()
        }else {
            resultBlock(CreditCardError(error: "No internet connection", message: "Try again!"))
            return
        }
    }
}
